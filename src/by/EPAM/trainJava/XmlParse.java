package by.EPAM.trainJava;

/***
 * считываем весь текст в огромную строку и ищем все узлы. 
 * Если узел не последний, то процедуру запуска повторяем рекурсивно. 
 * Из конечных узлов наверх поднимаем записи в виде имя=текст_узла
 * Не конечные узлы содержат в себе ссылку на дочерние. 
 * Т.е. основное хранилище для распарсенного XML это TreeMap
 * где ключами являются имена узлов, а значениями или тексты конечных узлов или ссылки на дочерние узлы. 
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlParse {
	String name;
	private Map <String,Object> mn;
	String tx;

	/***
	 * поиск открывающего/закрывающего тега и содержимого узла.
	 * открывающий тег это <((\\w.+).*?)>
	 * сразу используем 2 группы - первая это имя тега целиком, со свойствами
	 * вторая группа (\\w.+) это только имя тега. 
	 * Первая группа нам нужна для добавки в распарсенный файл
	 * Вторая чтобы по ней искать завершающий тег, который мы можем описать как ссылку на эту группу - </(\\2)>
	 * ну и третья группа - это весь текст между угловыми скобками (.+?)
	 */
	private static final Pattern patOpen = Pattern.compile("<((\\w.+).*?)>(.+?)</(\\2)>");
	
	XmlParse () {
		name="file0";
		mn=new TreeMap<>();
		tx= "";
	}

	/***
	 * основной конструктор
	 * @param nm имя файла
	 * @param txt текст файла
	 */
	XmlParse (String nm, String txt) {
		name=nm;
		mn=new TreeMap<>();
		tx= txt;
	}

	/***
	 * запуск парсинга. 
	 * текст зачитан при создании объекта
	 */
	public void parseStart () {
		mn=parseGrMap(tx.toString());
	}

	/***
	 * Основной метод парсинга
	 * @param t	текст для парсинга
	 * @return объект хранящий пары значений - имя узла/узел. узел может быть как текстом так и ссылкой на дочерние узлы.
	 */
	private Map <String,Object> parseGrMap (String t) {
		Matcher m = patOpen.matcher(t);
		Matcher mCheck;
		Map <String,Object> k=new TreeMap<>();
		
		while (m.find()) {
			mCheck= patOpen.matcher(m.group(3));
			if (mCheck.find()) {
				k.put(m.group(1),parseGrMap(m.group(3)));
			} else {
				k.put(m.group(1),m.group(3));
			}
		}
		return 	k;
	}
	
	/***
	 * но как вывести все в оригинальном порядке? подумаю потом, пока выводим просто сохраненную структуру
	 * тоже нужна рекурсия, т.к. глубина вложенности не известна
	 * @param sb куда накапливаем текст
	 * @param entryS запись из предыдущего уровня. может быть узлом, а может быть текстом
	 * @param level уровень чтобы красиво отступать при выводе вложенного уровня
	 */
	private void traversBack (StringBuilder sb, Map <String,Object> entryS, int level ){
		String folding= new String(new char[level]).replace("\0", "\t");
		for(Map.Entry<String,Object> entry : entryS.entrySet()) {
			// если конечный узел то выводим его со значением
			if (entry.getValue() instanceof String) {
				sb.append(folding+entry.getKey() + " => " + entry.getValue()+"\n");
			} else {
				// если не конечный узел то выводим только его имя
				sb.append(folding+entry.getKey() +"\n");
				// рекурсия. объект явно приводим к нашему типу 
				traversBack (sb, (Map <String,Object>)entry.getValue(), level+1);
			}	  
		}
	}
	
	@Override
	public String toString() {
		//return "XmlParse [name=" + name + ", mn=" + mn + "]";
		StringBuilder sb = new StringBuilder();
		sb.append("name=" + name+"\n");
		traversBack (sb,mn,0);
		
		return sb.toString();
	}	
	
		
}

